
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object index_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._
import ejisan.play.libs.PageMeta

class index extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[String,Seq[Task],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(message:String, tasks:Seq[Task]=Seq()):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.41*/("""



"""),_display_(/*5.2*/temp(message)/*5.15*/ {_display_(Seq[Any](format.raw/*5.17*/("""
    """),_display_(/*6.6*/taskList(tasks)),format.raw/*6.21*/("""
""")))}))
      }
    }
  }

  def render(message:String,tasks:Seq[Task]): play.twirl.api.HtmlFormat.Appendable = apply(message,tasks)

  def f:((String,Seq[Task]) => play.twirl.api.HtmlFormat.Appendable) = (message,tasks) => apply(message,tasks)

  def ref: this.type = this

}


}

/**/
object index extends index_Scope0.index
              /*
                  -- GENERATED --
                  DATE: Mon Oct 26 16:17:07 MDT 2015
                  SOURCE: E:/homework/scala_task_management/app/views/index.scala.html
                  HASH: 622f653dbdafeda99b9b7d464386c24092b45620
                  MATRIX: 570->1|704->40|734->45|755->58|794->60|825->66|860->81
                  LINES: 21->1|26->1|30->5|30->5|30->5|31->6|31->6
                  -- GENERATED --
              */
          