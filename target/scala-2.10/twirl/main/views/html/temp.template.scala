
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object temp_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._
import ejisan.play.libs.PageMeta

class temp extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[String,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(title: String):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.17*/(""" """),format.raw/*1.18*/("""(content:Html)
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
"""),_display_(/*9.2*/content),format.raw/*9.9*/("""
"""),format.raw/*10.1*/("""</body>
</html>"""))
      }
    }
  }

  def render(title:String): play.twirl.api.HtmlFormat.Appendable = apply(title)

  def f:((String) => play.twirl.api.HtmlFormat.Appendable) = (title) => apply(title)

  def ref: this.type = this

}


}

/**/
object temp extends temp_Scope0.temp
              /*
                  -- GENERATED --
                  DATE: Mon Oct 26 16:17:08 MDT 2015
                  SOURCE: E:/homework/scala_task_management/app/views/temp.scala.html
                  HASH: c0d2d4f47712c7741bfc23c25aef6d959aba6ff2
                  MATRIX: 558->1|668->16|696->17|839->135|865->142|893->143
                  LINES: 21->1|26->1|26->1|34->9|34->9|35->10
                  -- GENERATED --
              */
          