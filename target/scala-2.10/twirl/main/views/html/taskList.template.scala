
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object taskList_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._
import ejisan.play.libs.PageMeta

class taskList extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template1[Seq[Task],play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(tasks: Seq[Task]):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.20*/("""

"""),format.raw/*3.1*/("""<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src='"""),_display_(/*4.15*/routes/*4.21*/.Assets.versioned("scripts/Task.js")),format.raw/*4.57*/("""'></script>

<h1>Here are your tasks</h1>

"""),_display_(/*8.2*/helper/*8.8*/.form(action = routes.WebPage.deleteTask())/*8.51*/ {_display_(Seq[Any](format.raw/*8.53*/("""
"""),format.raw/*9.1*/("""<table id="list" style="border:solid 1px grey; text-align:left">
    <tr>
        <th>User</th>
        <th>Task Description</th>
    </tr>
    """),_display_(/*14.6*/tasks/*14.11*/.map/*14.15*/ {item =>_display_(Seq[Any](format.raw/*14.24*/("""
        """),format.raw/*15.9*/("""<tr>
            <td>"""),_display_(/*16.18*/item/*16.22*/.user.userName),format.raw/*16.36*/("""</td>
            <td>"""),_display_(/*17.18*/item/*17.22*/.description),format.raw/*17.34*/("""</td>
            <td><input id=""""),_display_(/*18.29*/item/*18.33*/.id),format.raw/*18.36*/("""" type="button" value="delete" onclick="deleteTaskById(this.id)"/></td>
        </tr>
    """)))}),format.raw/*20.6*/("""
"""),format.raw/*21.1*/("""</table>
""")))}),format.raw/*22.2*/("""


"""),_display_(/*25.2*/helper/*25.8*/.form(action = routes.WebPage.addTask())/*25.48*/ {_display_(Seq[Any](format.raw/*25.50*/("""
"""),format.raw/*26.1*/("""<input id="taskText" type="text" length="30"/>
<input type="button" value="Add" onclick="addTask('"""),_display_(/*27.53*/tasks(0)/*27.61*/.user.userName),format.raw/*27.75*/("""', $('#taskText').val())"/>
""")))}),format.raw/*28.2*/("""

"""))
      }
    }
  }

  def render(tasks:Seq[Task]): play.twirl.api.HtmlFormat.Appendable = apply(tasks)

  def f:((Seq[Task]) => play.twirl.api.HtmlFormat.Appendable) = (tasks) => apply(tasks)

  def ref: this.type = this

}


}

/**/
object taskList extends taskList_Scope0.taskList
              /*
                  -- GENERATED --
                  DATE: Mon Oct 26 16:17:07 MDT 2015
                  SOURCE: E:/homework/scala_task_management/app/views/taskList.scala.html
                  HASH: bb2b8ad95b30fa4b845d8ae19fc5dd03bda1990f
                  MATRIX: 569->1|682->19|710->21|813->98|827->104|883->140|952->184|965->190|1016->233|1055->235|1082->236|1253->381|1267->386|1280->390|1327->399|1363->408|1412->430|1425->434|1460->448|1510->471|1523->475|1556->487|1617->521|1630->525|1654->528|1775->619|1803->620|1843->630|1873->634|1887->640|1936->680|1976->682|2004->683|2130->782|2147->790|2182->804|2241->833
                  LINES: 21->1|26->1|28->3|29->4|29->4|29->4|33->8|33->8|33->8|33->8|34->9|39->14|39->14|39->14|39->14|40->15|41->16|41->16|41->16|42->17|42->17|42->17|43->18|43->18|43->18|45->20|46->21|47->22|50->25|50->25|50->25|50->25|51->26|52->27|52->27|52->27|53->28
                  -- GENERATED --
              */
          