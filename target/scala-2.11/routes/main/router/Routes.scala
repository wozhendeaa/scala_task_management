
// @GENERATOR:play-routes-compiler
// @SOURCE:E:/homework/scala_task_management/conf/routes
// @DATE:Tue Oct 27 13:37:08 MDT 2015

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  WebPage_1: controllers.WebPage,
  // @LINE:16
  WebJarAssets_0: controllers.WebJarAssets,
  // @LINE:18
  Assets_2: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    WebPage_1: controllers.WebPage,
    // @LINE:16
    WebJarAssets_0: controllers.WebJarAssets,
    // @LINE:18
    Assets_2: controllers.Assets
  ) = this(errorHandler, WebPage_1, WebJarAssets_0, Assets_2, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, WebPage_1, WebJarAssets_0, Assets_2, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.WebPage.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.WebPage.loginPage"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """register""", """controllers.WebPage.registerPage"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """login""", """controllers.WebPage.login"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """register""", """controllers.WebPage.register"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """addTask""", """controllers.WebPage.addTask"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """deleteTask""", """controllers.WebPage.deleteTask"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """register/validateUserName""", """controllers.WebPage.validateUserName"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/lib/wjars/$file<.+>""", """controllers.WebJarAssets.at(file:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_WebPage_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_WebPage_index0_invoker = createInvoker(
    WebPage_1.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WebPage",
      "index",
      Nil,
      "GET",
      """ Home page""",
      this.prefix + """"""
    )
  )

  // @LINE:7
  private[this] lazy val controllers_WebPage_loginPage1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_WebPage_loginPage1_invoker = createInvoker(
    WebPage_1.loginPage,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WebPage",
      "loginPage",
      Nil,
      "GET",
      """""",
      this.prefix + """login"""
    )
  )

  // @LINE:8
  private[this] lazy val controllers_WebPage_registerPage2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("register")))
  )
  private[this] lazy val controllers_WebPage_registerPage2_invoker = createInvoker(
    WebPage_1.registerPage,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WebPage",
      "registerPage",
      Nil,
      "GET",
      """""",
      this.prefix + """register"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_WebPage_login3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("login")))
  )
  private[this] lazy val controllers_WebPage_login3_invoker = createInvoker(
    WebPage_1.login,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WebPage",
      "login",
      Nil,
      "POST",
      """""",
      this.prefix + """login"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_WebPage_register4_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("register")))
  )
  private[this] lazy val controllers_WebPage_register4_invoker = createInvoker(
    WebPage_1.register,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WebPage",
      "register",
      Nil,
      "POST",
      """""",
      this.prefix + """register"""
    )
  )

  // @LINE:11
  private[this] lazy val controllers_WebPage_addTask5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("addTask")))
  )
  private[this] lazy val controllers_WebPage_addTask5_invoker = createInvoker(
    WebPage_1.addTask,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WebPage",
      "addTask",
      Nil,
      "GET",
      """""",
      this.prefix + """addTask"""
    )
  )

  // @LINE:12
  private[this] lazy val controllers_WebPage_deleteTask6_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("deleteTask")))
  )
  private[this] lazy val controllers_WebPage_deleteTask6_invoker = createInvoker(
    WebPage_1.deleteTask,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WebPage",
      "deleteTask",
      Nil,
      "GET",
      """""",
      this.prefix + """deleteTask"""
    )
  )

  // @LINE:13
  private[this] lazy val controllers_WebPage_validateUserName7_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("register/validateUserName")))
  )
  private[this] lazy val controllers_WebPage_validateUserName7_invoker = createInvoker(
    WebPage_1.validateUserName,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WebPage",
      "validateUserName",
      Nil,
      "POST",
      """""",
      this.prefix + """register/validateUserName"""
    )
  )

  // @LINE:16
  private[this] lazy val controllers_WebJarAssets_at8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/lib/wjars/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_WebJarAssets_at8_invoker = createInvoker(
    WebJarAssets_0.at(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.WebJarAssets",
      "at",
      Seq(classOf[String]),
      "GET",
      """ Map static resources from the web jars to the /assets/lib/wjars URL path""",
      this.prefix + """assets/lib/wjars/$file<.+>"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_Assets_versioned9_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned9_invoker = createInvoker(
    Assets_2.versioned(fakeValue[String], fakeValue[Asset]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      """ Map static resources from the /public folder to the /assets URL path""",
      this.prefix + """assets/$file<.+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_WebPage_index0_route(params) =>
      call { 
        controllers_WebPage_index0_invoker.call(WebPage_1.index)
      }
  
    // @LINE:7
    case controllers_WebPage_loginPage1_route(params) =>
      call { 
        controllers_WebPage_loginPage1_invoker.call(WebPage_1.loginPage)
      }
  
    // @LINE:8
    case controllers_WebPage_registerPage2_route(params) =>
      call { 
        controllers_WebPage_registerPage2_invoker.call(WebPage_1.registerPage)
      }
  
    // @LINE:9
    case controllers_WebPage_login3_route(params) =>
      call { 
        controllers_WebPage_login3_invoker.call(WebPage_1.login)
      }
  
    // @LINE:10
    case controllers_WebPage_register4_route(params) =>
      call { 
        controllers_WebPage_register4_invoker.call(WebPage_1.register)
      }
  
    // @LINE:11
    case controllers_WebPage_addTask5_route(params) =>
      call { 
        controllers_WebPage_addTask5_invoker.call(WebPage_1.addTask)
      }
  
    // @LINE:12
    case controllers_WebPage_deleteTask6_route(params) =>
      call { 
        controllers_WebPage_deleteTask6_invoker.call(WebPage_1.deleteTask)
      }
  
    // @LINE:13
    case controllers_WebPage_validateUserName7_route(params) =>
      call { 
        controllers_WebPage_validateUserName7_invoker.call(WebPage_1.validateUserName)
      }
  
    // @LINE:16
    case controllers_WebJarAssets_at8_route(params) =>
      call(params.fromPath[String]("file", None)) { (file) =>
        controllers_WebJarAssets_at8_invoker.call(WebJarAssets_0.at(file))
      }
  
    // @LINE:18
    case controllers_Assets_versioned9_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned9_invoker.call(Assets_2.versioned(path, file))
      }
  }
}