
// @GENERATOR:play-routes-compiler
// @SOURCE:E:/homework/scala_task_management/conf/routes
// @DATE:Tue Oct 27 13:37:08 MDT 2015


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
