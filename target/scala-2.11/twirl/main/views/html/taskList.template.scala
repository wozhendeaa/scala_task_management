
package views.html

import play.twirl.api._
import play.twirl.api.TemplateMagic._


     object taskList_Scope0 {
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._
import ejisan.play.libs.PageMeta

class taskList extends BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with play.twirl.api.Template2[Seq[Task],play.api.mvc.Session,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(tasks: Seq[Task])(implicit session: play.api.mvc.Session):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*1.60*/("""

"""),format.raw/*3.1*/("""<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src='"""),_display_(/*4.15*/routes/*4.21*/.Assets.versioned("scripts/Task.js")),format.raw/*4.57*/("""'></script>

<h1>Here are your tasks</h1>

"""),_display_(/*8.2*/helper/*8.8*/.form(action = routes.WebPage.deleteTask())/*8.51*/ {_display_(Seq[Any](format.raw/*8.53*/("""
"""),format.raw/*9.1*/("""<table id="list" style="border:solid 1px grey; text-align:left">
    <tr>
        <th>User</th>
        <th>Task Description</th>
    </tr>
    """),_display_(/*14.6*/tasks/*14.11*/.map/*14.15*/ {item =>_display_(Seq[Any](format.raw/*14.24*/("""
        """),format.raw/*15.9*/("""<tr>
            <td>"""),_display_(/*16.18*/item/*16.22*/.user.userName),format.raw/*16.36*/("""</td>
            <td>"""),_display_(/*17.18*/item/*17.22*/.description),format.raw/*17.34*/("""</td>
            <td><input id=""""),_display_(/*18.29*/item/*18.33*/.id),format.raw/*18.36*/("""" type="button" value="delete" onclick="deleteTaskById(this.id)"/></td>
        </tr>
    """)))}),format.raw/*20.6*/("""
"""),format.raw/*21.1*/("""</table>
""")))}),format.raw/*22.2*/("""


"""),_display_(/*25.2*/helper/*25.8*/.form(action = routes.WebPage.addTask())/*25.48*/ {_display_(Seq[Any](format.raw/*25.50*/("""
"""),format.raw/*26.1*/("""<input id="taskText" type="text" length="30"/>
<input type="button" value="Add" onclick="addTask('"""),_display_(/*27.53*/session/*27.60*/.get("user")),format.raw/*27.72*/("""', $('#taskText').val())"/>
""")))}),format.raw/*28.2*/("""

"""))
      }
    }
  }

  def render(tasks:Seq[Task],session:play.api.mvc.Session): play.twirl.api.HtmlFormat.Appendable = apply(tasks)(session)

  def f:((Seq[Task]) => (play.api.mvc.Session) => play.twirl.api.HtmlFormat.Appendable) = (tasks) => (session) => apply(tasks)(session)

  def ref: this.type = this

}


}

/**/
object taskList extends taskList_Scope0.taskList
              /*
                  -- GENERATED --
                  DATE: Tue Oct 27 12:22:06 MDT 2015
                  SOURCE: E:/homework/scala_task_management/app/views/taskList.scala.html
                  HASH: da3b2a1a70e16b68d973b975a0442be2434aa6ee
                  MATRIX: 590->1|743->59|771->61|874->138|888->144|944->180|1013->224|1026->230|1077->273|1116->275|1143->276|1314->421|1328->426|1341->430|1388->439|1424->448|1473->470|1486->474|1521->488|1571->511|1584->515|1617->527|1678->561|1691->565|1715->568|1836->659|1864->660|1904->670|1934->674|1948->680|1997->720|2037->722|2065->723|2191->822|2207->829|2240->841|2299->870
                  LINES: 21->1|26->1|28->3|29->4|29->4|29->4|33->8|33->8|33->8|33->8|34->9|39->14|39->14|39->14|39->14|40->15|41->16|41->16|41->16|42->17|42->17|42->17|43->18|43->18|43->18|45->20|46->21|47->22|50->25|50->25|50->25|50->25|51->26|52->27|52->27|52->27|53->28
                  -- GENERATED --
              */
          