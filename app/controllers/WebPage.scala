package controllers

import javax.inject.Singleton

import models.{Task, User}
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json._
import play.api.mvc._


@Singleton
class WebPage extends Controller{
  var uid = 5;
  var tid = 4;

    val userForm : Form[User] = Form {
    mapping(
//      "uid" -> optional(number),
      "username" -> nonEmptyText(1, 10),
      "password" -> nonEmptyText(1, 15)
    ) (User.apply)(User.unapply)
  }


  /** Home page */
  def index = Action { implicit request =>
    val user = request.session.get("user")

    user.map {u =>

      Ok(views.html.taskList(Task.getTasksByUser(user.get)))

    }.getOrElse {
      Ok(views.html.login("You need to login first"))
    }
  }

  def loginPage = Action {
    Ok(views.html.login(""))
  }

  def registerPage = Action {
    Ok(views.html.register(userForm))

  }

  def login = Action { implicit request =>
    val form = userForm.bindFromRequest().get

    val username = form.userName
    val password = form.password
    val user = User.findUser(username, password)

    if(user.isDefined) {
      Redirect(routes.WebPage.index).withSession(
      "user" -> username
      )
    } else {
      Ok(views.html.login("incorrect username or password"))
    }
  }

  def register = Action { implicit request =>
    val user = userForm.bindFromRequest().get

    val newUser = User(user.userName, user.password)
    User.addUser(newUser)

    Redirect(routes.WebPage.index()).withSession(
    "user" -> newUser.userName
    )
  }

  def addTask = Action {implicit request =>
    val uname = request.getQueryString("uname").get
    val text = request.getQueryString("text").get

    val user = User.findUser(uname)
    tid = tid + 1
    val task = Task(tid, user.get,text)
    Task.addTask(task)
    Redirect(routes.WebPage.index)
  }

  def deleteTask = Action { implicit request =>
    val id: Int = request.getQueryString("id").get.toInt
    Redirect(routes.WebPage.index)
  }


  def validateUserName = Action { implicit request =>
    val name : String = request.getQueryString("username").get
    val valid = User.findUser(name).isDefined

    Ok(Json.toJson(!valid))
  }

}
