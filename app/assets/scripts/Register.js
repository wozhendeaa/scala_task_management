
function validateform() {
    $('#usernameError').text("");
    $('#passwordError').text("");
    $('#confirmPasswordError').text("");

    var valid = true;
    var validUserNameLength = 6;
    var validPasswordLength = 6;

    var username = $('#username').val();
    if(username.length < validUserNameLength) {
        valid = false;
        $('#usernameError').text("user name needs to be 8 characters long");
    }

     $.ajax({
        type:"post",
        url:"/register/validateUserName?username=" + username,
        success: function(data) {
            if(data === false) {
                valid = false;
                console.log(data);
                $('#usernameError').text("username already exists");
            }
        }
      });

  if($('#password').val().length < validPasswordLength) {
        valid = false;
        $('#passwordError').text("password needs to be 8 characters long");

   }

  if($('#password').val() != $('#confirmPassword').val()) {
        valid = false;
        $('#confirmPasswordError').text("password and confirm password are different");

  }

  if(valid) {
    var form = $('#registerForm');
    console.log("submit");
    form.submit();
  }
}
