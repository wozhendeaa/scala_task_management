package models

/**
 * Created by Yi on 10/22/2015.
 */
case class Task (
 id:Int, user: User, description:String){

}


object Task
{

 val users = User.getUsers

  var tasks = Seq(
   Task(0, users(0), "kongfu"),
   Task(1, users(1), "Lasering"),
   Task(2, users(2), "shoooting practice"),
   Task(3, users(3), "mood control")
  )

 def getTasks = {
  tasks
 }

 def addTask(task: Task) = {
  tasks = tasks :+ task
 }

 def deleteTaskById(id : Int) = {
  tasks = tasks.filter(task => task.id != id)
 }

 def getTasksByUser(username : String) : Seq[Task] = {
   tasks.filter(_.user.userName == username)
 }
}