package models

/**
 * Created by Yi on 10/22/2015.
 */
case class User (userName: String, password: String) {


}

object User
{

  var users = Seq(
    User( "Batman", "BATPassword"),
    User( "Superman", "Flying"),
    User( "Hawkeye", "shooooot"),
    User( "Hulk", "Smashhhh")
  )

  def addUser(user: User) = {
    users = users :+ user
  }

  def getUsers = {
    users
  }

  def findUser(uname : String, psw: String) : Option[User] = {
    val user: Option[User] = users.filter(_.userName == uname).filter(_.password == psw).headOption
    user
  }

  def findUser(uname : String) : Option[User] = {
    val user: Option[User] = users.filter(_.userName == uname).headOption
    user
  }
}